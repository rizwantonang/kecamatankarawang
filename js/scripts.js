(function ($) {

	'use strict';

	$(window).load( function() {
		// FLEXSLIDER
		$('#carousel').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    itemWidth: 85,
		    itemMargin: 0,
		    asNavFor: '#slider',
			prevText: "",
			nextText: "",  
		});

		$('#slider').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    sync: "#carousel",
			prevText: "",
			nextText: "",  
		});
	});


	// PRETTYPHOTO SETUP
	$(".subitem-article-gallery a[rel^='prettyPhoto'] ").prettyPhoto({
		animation_speed: 'normal',
		hideflash: true,
		default_width: 800,
	});

	// RWD-CONTENT-SIDEBAR
	$(".left-sidebar .widget").clone(false).appendTo( $(".rwd-content-sidebar") );
	$(".right-sidebar .widget").clone(false).appendTo( $(".rwd-content-sidebar") );

	// RWD-MAIN-NAVIGATION
	$(".main-navigation > ul").clone(false).find("ul,li").removeAttr("id").remove(".submenu").appendTo( $("#nav-rwd-left > ul") );

	// RWD-HANDLERS
	$(".btn-hide, .btn-show").click( function(e){
		var rwd_sidebar = $(".nav-rwd-sidebar");
		var rwd_wrapper = $(".wrapper-inner");

		rwd_sidebar.toggleClass("rwd-sidebar-active");
		rwd_wrapper.toggleClass("on-active-rwd");
	});

})(jQuery);